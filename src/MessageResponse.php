<?php

namespace Sabim\DataGrid;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function getAdditional():Array{
        return [];
    }
    public function toArray(Request $request): array
    {
        return [
            'message' =>$this->when($this['message']??null, $this['message']??null),
            'data' => Arr::wrap($this['data']??null)??[],
            'errors' => $this->when($this['errors']??null, Arr::wrap($this['errors']??null)??[]),
            'additional'=>$this->getAdditional()
        ];
    }
}
