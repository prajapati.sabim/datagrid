<?php
namespace Sabim\DataGrid;

use Carbon\Carbon;
use Illuminate\Http\Request;

class DataGrid 
{
    protected $model;
    protected $request;
    protected $datas;
    protected  $additional=[];
    public function __construct(Request $request,$model,$query)
    {
        $this->request=$request;
        $this->model=$model;
        $this->datas=$query;
    }

    public function Change($datas)
    {
        $this->datas=$datas;
    }
    public function setAdditional($key,$datas)
    {
        $this->additional[$key]=$datas;
    }
    public function extra():Array
    {
        return [];
    }

    public function call()
    {
        $datas=$this->datas;
        $filter_ables=$this->model::getFilter();
        $search_ables=$this->model::getSearch();
        $sort_ables=$this->model::getSort();
        $columns=$this->model::getColumn();

        $limit_val = $this->request['per_page']??10;
        
        $orderMap=['asc','desc'];

        $orders = $this->request['order'];
        
        $search_value=$this->request['search'];
        
        $filters=$this->request['filter'];

        if($search_value){            
            $datas =  $datas->where(function($query) use ($search_ables,$search_value){
                foreach ($search_ables as $key => $value) {
                    $query->orWhere($value, 'like', "%{$search_value}%");
                }
            });            
        }
        // dd($filter_ables['gender_id']['foreign_class']::getTableName());
        if($filters){ 
            if(is_array($filters)){
                foreach ($filters as $key => $filter) {
                    if(in_array($key,array_keys($filter_ables))){
                        $tmp_filter=$filter_ables[$key];
                        if($tmp_filter['foreign']){
                            $datas->whereHas($tmp_filter['relations'], function($query1) use($tmp_filter,$key,$filter)
                            {
                                if(is_array($filter)){
                                    if($tmp_filter['type']=='date'){  
                                        $start=Carbon::parse($filter[0])->format('Y-m-d');                                                  
                                        $end=Carbon::parse($filter[1])->format('Y-m-d');                                                   
                                        $query1->where($key,'>=',$start)->where($key,'<=',$end);
                                    }else{
                                        $query1->whereIn($key,$filter);
                                    }
                                }else{
                                    $query1->where($key,$filter);
                                }        
                            });
                        }else{   
                            if(isset($tmp_filter['boolean'])&&$tmp_filter['boolean']){
                                if($filter=="True"){
                                    $datas->whereNotNull($key);
                                }else{
                                    $datas->whereNull($key);
                                }
                            }else{
                                if(is_array($filter)){
                                    if($tmp_filter['type']=='date'){  
                                        $start=Carbon::parse($filter[0])->format('Y-m-d');                                                  
                                        $end=Carbon::parse($filter[1])->format('Y-m-d 23:59:59');                                                   
                                        $datas->where($key,'>=',$start)->where($key,'<=',$end);
                                    }else{
                                        $datas->whereIn($key,$filter);
                                    }
                                }else{
                                    $datas->where($key,$filter);
                                }    
                            }                                            
                        }
                    }   
                }
            }        
        }
        if($orders){  
            if(is_array($orders)){
                foreach ($orders as $key => $order) {
                    if(in_array($key,$sort_ables)){
                        if(!in_array($order,$orderMap)) $order='desc';
                        $datas->orderBy($key,$order);
                    }   
                }
            }          
        }else{
            $default_sorts=$this->model::getDefaultSort();
            if($default_sorts){
                if(is_array($default_sorts)){
                    foreach ($default_sorts as $key => $order) {
                        if(!in_array($order,$orderMap)) $order='desc';
                        $datas->orderBy($key,$order);
                    }
                }
            }
        }
        
        $response= new ApiResponse($datas->paginate($limit_val));
        if($filter_ables){
            foreach ($filter_ables as $key => &$filter_able) {
                if(!isset($filter_able['hidden'])||!$filter_able['hidden']){
                    if($filter_able['type']=='select'){
                        if(!$filter_able['data']){
                            $f_data=$filter_able['data_class']::pluck($filter_able['data_value'],$filter_able['data_key'])->toArray();
                            $filter_able['data']=$f_data;
                        }
                    }
                    $remove =['relations','foreign','data_class','data_key','data_value'];
    
                    $filter_able = array_diff_key($filter_able, array_flip($remove));
                }else{
                    unset($filter_ables[$key]);
                }
            }
        }
        $this->additional["config"]= [
            'sortable'=>$sort_ables,
            'filterable'=>$filter_ables,           
            'searchable'=>$search_ables ,           
            'columns'=>$columns ,           
        ];
        $response->additional($this->additional);

        //addextra

        return $response;
    }
}
?>